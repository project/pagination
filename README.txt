
Pagination
----------------
To install, place the entire "pagination" folder within your modules directory. Under Administer -> Site Building -> Modules, enabled the pagination module.

You may now select which node types you wish to enable pagination on by navigating to Administer -> Site Configuration -> Pagination. Finally, you may now choose a pagination option for selected node types under Administration -> Content Management -> Content Types -> [Selected Node Type] -> Submission Form Settings.

Maintainer
owen [at] mundanity [dot] com